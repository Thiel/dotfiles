now=$(date +%Y%m%d%H%M%S)

# deplay(file dest)
function deploy() {
    SRC=$1
    DST=$2
    [ -f $DST ] && cp $DST .emacs.$now && rm $DST
    ln -s $(pwd)/$SRC $DST
}

deploy bashrc ~/.bashrc
! [ -f ~/.bash_profile ] && echo "source ~/.bashrc" > .bash_profile # fix tmux issue
echo "set completion-ignore-case On" >> ~/.inputrc # case insentive completion for bash

deploy emacs ~/.emacs
deploy tmux.conf ~/.tmux.conf
deploy sshrc ~/.ssh/rc
