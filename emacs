(setq package-enable-at-startup nil)
;; define /tmp as auto-save file directory
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

(global-set-key [M-delete] 'kill-word)

(autoload 'glsl-mode "glsl-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.glsl\\'" . glsl-mode))
(add-to-list 'auto-mode-alist '("\\.vert\\'" . glsl-mode))
(add-to-list 'auto-mode-alist '("\\.frag\\'" . glsl-mode))
(add-to-list 'auto-mode-alist '("\\.geom\\'" . glsl-mode))

;; Testing improved workflow
(require 'ido)
(add-to-list 'ido-work-directory-list-ignore-regexps tramp-file-name-regexp)
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)

(autoload 'lua-mode "lua-mode" "Lua editing mode." t)
(add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))
(add-to-list 'interpreter-mode-alist '("lua" . lua-mode))

(require 'tex-mode)
(add-hook 'latex-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)   ; with AUCTeX LaTeX mode
(add-hook 'latex-mode-hook 'turn-on-reftex)   ; with Emacs latex mode
;(setq ispell-dictionary "fr")

(require 'ansi-color)
(defun colorize-compilation-buffer ()
  (toggle-read-only)
  (ansi-color-apply-on-region (point-min) (point-max))
  (toggle-read-only))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)

;; show matching parenteses
(show-paren-mode 1)

;; indentation style
(setq-default indent-tabs-mode nil)
(setq c-default-style "bsd"
      c-basic-offset 2)

;; avoid tags file path prompt
(setq tags-file-name "TAGS")

;; set M-q (fill paragraphe) at 80 column
(setq-default fill-column 80)

;;set 80 column rule
(require 'whitespace)
(setq whitespace-style '(face empty trailing lines-tail))
(global-whitespace-mode t)

(defun insert-header-guard ()
  (interactive)
  (save-excursion
    (when (buffer-file-name)
      (let*
          ((name (file-name-nondirectory buffer-file-name))
           (macro (replace-regexp-in-string
                   "\\." "_"
                   (replace-regexp-in-string
                    "-" "_"
                    (upcase name)))))
        (goto-char (point-min))
        (insert "#ifndef " macro "\n")
        (insert "# define " macro "\n\n")
        (goto-char (point-max))
        (insert "\n#endif /* !"macro" */\n")))))
(add-hook 'find-file-hooks
          (lambda ()
            (when (and (memq major-mode '(c-mode c++-mode))
                       (equal (point-min) (point-max))
                       (string-match ".*\\.hh?" (buffer-file-name)))
              (insert-header-guard)
              (goto-line 3)
              (insert "\n"))))

(global-set-key (kbd "C-c c") 'recompile)
(global-set-key (kbd "C-c v") 'insert-header-guard)
(global-set-key (kbd "C-c v") 'insert-header-guard)
(global-set-key (kbd "C-c f") 'ff-find-other-file)


(setq compilation-always-kill t)

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
(package-initialize)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (tango-dark))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(add-hook 'c-mode-common-hook
          (lambda ()
            (if (derived-mode-p 'c-mode 'c++-mode)
                (cppcm-reload-all)
              )))

(setq auto-mode-alist
        (append
            '(("CMakeLists\\.txt\\'" . cmake-mode))
               '(("\\.cmake\\'" . cmake-mode))
                  auto-mode-alist))
(setq cppcm-write-flymake-makefile nil)

(deftheme custom
  "My custom faces that fix some theme annoyances.")
(custom-theme-set-faces 'custom
 `(company-tooltip ((t :background "lightgray" :foreground "black")))
 `(company-tooltip-selection ((t :background "steelblue" :foreground "white")))
 `(company-tooltip-mouse ((t :background "blue" :foreground "white")))
 `(company-tooltip-common ((t :background "lightgray" :foreground "black")))
 `(company-tooltip-common-selection ((t t :background "lightgray" :foreground "black")))
 ;; `(company-tooltip-annotation ((t :background "" :foreground "")))
 `(company-scrollbar-fg ((t :background "black")))
 `(company-scrollbar-bg ((t :background "gray")))
 `(company-preview ((t :background nil :foreround "darkgray")))
 `(company-preview-common ((t :background nil :foreground "darkgray")))
 ;; `(company-preview-search ((t :background "" :foreground "")))
 )


(add-hook 'c-mode-common-hook
          (lambda ()
            (if (derived-mode-p 'c-mode 'c++-mode)
                (cppcm-reload-all)
              )))

(require 'company)
(add-hook 'after-init-hook 'global-company-mode)
;(add-hook 'c-mode-common-hook 'company-mode)

(setq auto-mode-alist
        (append
            '(("CMakeLists\\.txt\\'" . cmake-mode))
               '(("\\.cmake\\'" . cmake-mode))
                  auto-mode-alist))

(eval-after-load 'company
  '(add-to-list 'company-backends 'company-irony))

;; (optional) adds CC special commands to `company-begin-commands' in order to
;; trigger completion at interesting places, such as after scope operator
;;     std::|
(add-hook 'irony-mode-hook 'company-irony-setup-begin-commands)


(require 'clang-format)
(global-set-key [C-M-tab] 'clang-format-region)
